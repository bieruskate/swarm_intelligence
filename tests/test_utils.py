from room.utils import angle_between


class TestUtils:
    def test_should_compute_angle_between_2_points(self):
        a = (0, 0)
        b = (1, 1)

        assert angle_between(a, b) == 225
        assert angle_between(b, a) == 45
