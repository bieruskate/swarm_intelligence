from room.furniture import Orientation
from room.room import Furniture, Room


class TestFurniture:
    def test_should_calculate_furniture_coordinates(self):
        table = Furniture(center=(3, 2), width=4, height=2)

        expected_coords = {
            'bottom_left': (1, 1),
            'bottom_right': (5, 1),
            'top_left': (1, 3),
            'top_right': (5, 3)
        }

        coords = table.get_coordinates()

        assert expected_coords == coords

    def test_should_calculate_coordinates_when_orientation_is_changed(self):
        table = Furniture(center=(3, 2), width=4, height=2, orientation=Orientation.VERTICAL)

        expected_coords = {
            'bottom_left': (2, 0),
            'bottom_right': (4, 0),
            'top_left': (2, 4),
            'top_right': (4, 4)
        }

        coords = table.get_coordinates()

        assert expected_coords == coords

    def test_furniture_overlap_vertically(self):
        table = Furniture(center=(2, 2), width=4, height=4)
        armchair = Furniture(center=(4, 1), width=2, height=2)
        assert table.overlap(armchair)

    def test_furniture_overlap_horizontally(self):
        table = Furniture(center=(2, 2), width=4, height=4)
        armchair = Furniture(center=(1, 4), width=2, height=2)
        assert table.overlap(armchair)

    def test_furniture_do_not_overlap(self):
        table = Furniture(center=(2, 2), width=4, height=4)
        armchair = Furniture(center=(5, 1), width=2, height=2)
        assert not table.overlap(armchair)

    def test_check_if_room_is_valid_counts_overlaps(self):
        furniture = [
            Furniture(center=(2, 2), width=4, height=4),
            Furniture(center=(1, 4), width=2, height=2),
            Furniture(center=(4, 1), width=2, height=2)]
        room = self._create_room(furniture)

        room.is_current_furniture_placement_legal()

        assert room.overlaps == 2

    def test_check_if_room_is_valid_counts_overlaps_if_none(self):
        furniture = [
            Furniture(center=(2, 2), width=4, height=4),
            Furniture(center=(5, 1), width=2, height=2)]
        room = self._create_room(furniture)

        room.is_current_furniture_placement_legal()

        assert room.overlaps == 0

    def test_furniture_outside_room_vertically(self):
        furniture = [Furniture(center=(2, 1), width=4, height=4)]
        room = self._create_room(furniture)
        assert not room.is_furniture_inside_room(furniture[0])

    def test_furniture_outside_room_horizontally(self):
        furniture = [Furniture(center=(1, 2), width=4, height=4)]
        room = self._create_room(furniture)
        assert not room.is_furniture_inside_room(furniture[0])

    def test_furniture_inside_room(self):
        furniture = [Furniture(center=(2, 2), width=4, height=4)]
        room = self._create_room(furniture)
        assert room.is_furniture_inside_room(furniture[0])

    def test_check_if_room_is_valid_counts_outsides(self):
        furniture = [
            Furniture(center=(1, 2), width=4, height=4),
            Furniture(center=(2, 1), width=4, height=4)]
        room = self._create_room(furniture)

        room.is_current_furniture_placement_legal()

        assert room.furniture_outside == 2

    def test_check_if_room_is_valid_counts_outsides_if_none(self):
        furniture = [Furniture(center=(2, 2), width=4, height=4)]
        room = self._create_room(furniture)

        room.is_current_furniture_placement_legal()

        assert room.furniture_outside == 0

    def test_should_return_true_if_window_is_covered(self):
        furniture = [Furniture(center=(2, 10), width=4, height=6)]
        room = self._create_room(furniture)
        assert room.is_window_overlapped(1)

    def test_should_return_false_if_window_is_not_covered(self):
        furniture = [Furniture(center=(8, 10), width=4, height=6)]
        room = self._create_room(furniture)
        assert not room.is_window_overlapped(3)

    def test_should_return_true_if_door_is_covered(self):
        furniture = [Furniture(center=(21, 10), width=4, height=6)]
        room = self._create_room(furniture)
        assert room.is_door_overlapped(6)

    def test_should_return_false_if_door_is_not_covered(self):
        furniture = [Furniture(center=(21, 10), width=4, height=6)]
        room = self._create_room(furniture)
        assert not room.is_door_overlapped(4)

    def _create_room(self, furniture):
        room = Room(
            26, 17, furniture,
            window=((0, 5), (0, 13)), door=((20, 17), (24, 17)))
        return room
