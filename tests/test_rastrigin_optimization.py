from benchmark import RastriginOptimization, HyperCubeRastriginOptimization


class TestRastriginOptimization:
    def test_should_return_fitness_value(self):
        opt = RastriginOptimization()

        assert opt.get_fitness_value([1, 2]) == 5
        assert opt.get_fitness_value([2, 6, 4, 2]) == 60
        assert opt.get_fitness_value([-1.5]) == 22.25


class TestHyperCubeRastriginOptimization:
    @classmethod
    def setup_class(cls):
        cls.opt = HyperCubeRastriginOptimization()

    def test_should_return_negative_value_when_out_of_bounds(self):
        assert self.opt.get_fitness_value([-6, 5]) == -1
        assert self.opt.get_fitness_value([-2, -3, 4, 10]) == -1

    def test_should_return_fitness_value_when_in_bounds(self):
        assert self.opt.get_fitness_value([1, 2]) == 5
