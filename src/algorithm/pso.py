from abc import ABC, abstractmethod
from copy import copy
from operator import attrgetter

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation

from benchmark import RastriginOptimization, HyperCubeRastriginOptimization
from furniture import Furniture
from room.room import Room


class AbstractParticle(ABC):
    def __init__(self, c1, c2, inertia):
        self.c1 = c1
        self.c2 = c2
        self.inertia = inertia

        self.state = self.init_state()
        self.optimizer = self.get_optimizer()
        self.p_best_fitness = self.get_initial_fitness()
        self.p_best_state = self.state

        self.velocity = np.zeros_like(self.state)

    @abstractmethod
    def init_state(self):
        """Returns initial vector. Should be overrided."""
        pass

    @abstractmethod
    def get_initial_fitness(self):
        """Returns initial fitness. Typically should be 0 or inf (depending on optimization problem)."""
        pass

    @abstractmethod
    def get_optimizer(self):
        """Returns optimizer instance (has to implement Problem interface)."""
        pass

    def calculate_fitness(self):
        return self.optimizer.get_fitness_value(self.state)

    def update_velocity(self, g_best):
        self.velocity = self.velocity * self.inertia + \
                        self.c1 * np.random.uniform() * (self.p_best_state - self.state) + \
                        self.c2 * np.random.uniform() * (g_best.p_best_state - self.state)

    def move(self):
        self.state += self.velocity


class RastriginParticle(AbstractParticle):
    def __init__(self, c1, c2, inertia, pk, **kwargs):
        self.dim = kwargs['dim']
        super().__init__(c1, c2, inertia)
        self.pk = pk

    def init_state(self):
        return np.random.uniform(-500, 500, self.dim)

    def get_optimizer(self):
        return RastriginOptimization()

    def get_initial_fitness(self):
        return np.inf


class HyperCubeRastriginParticle(RastriginParticle):
    def init_state(self):
        return np.random.uniform(-5.14, 5.14, self.dim)

    def get_optimizer(self):
        return HyperCubeRastriginOptimization()


class RoomParticle(AbstractParticle):
    def __init__(self, c1, c2, inertia, pk):
        self.furniture = self._set_up_furniture()
        self.pk = pk
        self.room_width = 40
        self.room_height = 30

        super().__init__(c1, c2, inertia)

    def init_state(self):
        for f in self.furniture:
            f.center = (np.random.uniform(0, self.room_width), np.random.uniform(0, self.room_height))

        centers = np.array([np.array(f.center) for f in self.furniture])
        return centers

    def get_optimizer(self):
        return Room(
            self.room_width, self.room_height, self.furniture,
            window=((0, 5), (0, 20)),
            door=((30, self.room_height), (38, self.room_height))
        )

    def get_initial_fitness(self):
        return - np.inf

    @staticmethod
    def _set_up_furniture():
        return [
            Furniture(center=(), width=10, height=4, name='Sofa'),
            Furniture(center=(), width=8, height=2, name='Television cabinet', can_stand_on_carpet=False),
            Furniture(center=(), width=7, height=7, name='Table with chairs', can_stand_on_carpet=False),
            Furniture(center=(), width=2, height=2, name='Armchair'),
            Furniture(center=(), width=6, height=1.5, name='Bookcase', can_stand_on_carpet=False),
            Furniture(center=(), width=4, height=2, name='Closet', can_stand_on_carpet=False),
            Furniture(center=(), width=8, height=2, name='Wardrobe', can_stand_on_carpet=False),
            Furniture(center=(), width=8, height=4, name='Coffee table'),
            Furniture(center=(), width=2, height=1, name='Clock', can_stand_on_carpet=False),
            Furniture(center=(), width=2, height=2, name='Sculpture')
        ]


class PSO:
    def __init__(self, size, particle_class, optimization_type, c_factors, inertia, **kwargs):
        c1, c2 = c_factors
        self.swarm = [particle_class(c1=c1, c2=c2, inertia=inertia, pk=i, **kwargs) for i in range(size)]
        self.optimization_type = optimization_type

    def find_best_particle(self):
        return self.optimization_type(self.swarm, key=attrgetter('p_best_fitness'))

    def have_better_fitness(self, f1, f2):
        return {min: f1 < f2,
                max: f1 > f2}[self.optimization_type]

    def run(self, not_improving_iters):
        previous_best_fitness = 0
        counter = 0

        while counter < not_improving_iters:

            g_best = self.step()

            if previous_best_fitness == g_best.p_best_fitness:
                counter += 1
            else:
                counter = 0

            previous_best_fitness = g_best.p_best_fitness

            # print(g_best.pk, g_best.p_best_fitness, counter)

        g_best.optimizer.get_fitness_value(g_best.p_best_state)
        return g_best

    def step(self):
        for particle in self.swarm:
            fitness = particle.calculate_fitness()

            if self.have_better_fitness(fitness, particle.p_best_fitness):
                particle.p_best_fitness = fitness
                particle.p_best_state = copy(particle.state)

        g_best = self.find_best_particle()

        for particle in self.swarm:
            particle.update_velocity(g_best)
            particle.move()

        return g_best


def draw_particles(i, pso):
    data = [particle.state for particle in pso.swarm]
    particles.set_offsets(data)

    best = pso.step()
    print(best.pk, best.p_best_fitness, best.p_best_state, i)
    return particles,


def create_animation(filename, pso, frames):
    global particles
    axes_limit = 800
    fig = plt.figure()
    ax = fig.add_subplot(111, aspect='equal', autoscale_on=False,
                         xlim=(-axes_limit, axes_limit), ylim=(-axes_limit, axes_limit))
    particles = ax.scatter([], [], s=5, animated=True)
    ani = animation.FuncAnimation(fig, draw_particles, frames=frames, blit=True, fargs=(pso,))
    ani.save(filename, fps=25, extra_args=['-vcodec', 'libx264'])


def run_room_optimization(swarm_size, c_factors, inertia, iters):
    ff = PSO(swarm_size, particle_class=RoomParticle, optimization_type=max,
             c_factors=c_factors, inertia=inertia)

    room = ff.run(not_improving_iters=iters).optimizer

    print('Is placement legal: ', room.is_current_furniture_placement_legal())
    print(f'Angle between sofa and TV: {room.get_angle_between_furniture(room.furniture[0], room.furniture[1]):.{5}}')
    print(f'Fitness: {room.get_fitness_value()}')
    room.draw_room()


def run_create_animation(swarm_size, c_factors, inertia, frames, filename):
    pso = PSO(swarm_size, particle_class=RastriginParticle, optimization_type=min,
              c_factors=c_factors, inertia=inertia, dim=2)

    create_animation(filename, pso, frames)


if __name__ == '__main__':
    run_room_optimization(50, c_factors=(2, 2), inertia=.6, iters=20)
    # run_create_animation(100, c_factors=(2, 2), inertia=.6, frames=300, filename='videos/pso.mp4')
