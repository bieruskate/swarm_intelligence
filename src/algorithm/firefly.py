from abc import ABC, abstractmethod
from copy import copy
from operator import attrgetter

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import animation

from benchmark import RastriginOptimization, HyperCubeRastriginOptimization
from furniture import Furniture
from src.room.room import Room


class AbstractFirefly(ABC):
    def __init__(self, beta, gamma, alpha, scale):
        self.beta = beta
        self.gamma = gamma
        self.alpha = alpha
        self.scale = scale

        self.state = self.init_state()
        self.optimizer = self.get_optimizer()
        self.best_fitness = self.get_initial_fitness()
        self.best_state = self.state

        self.fitness = self.best_fitness

    @abstractmethod
    def init_state(self):
        """Returns initial vector. Should be overrided."""
        pass

    @abstractmethod
    def get_initial_fitness(self):
        """Returns initial fitness. Typically should be -inf or inf (depending on optimization problem)."""
        pass

    @abstractmethod
    def get_optimizer(self):
        """Returns optimizer instance (has to implement Problem interface)."""
        pass

    def calculate_fitness(self):
        return self.optimizer.get_fitness_value(self.state)

    def update_fitness(self):
        self.fitness = self.optimizer.get_fitness_value(self.state)

    def move(self, better_firefly):
        self.state += self.beta / (1 + self.gamma * self._get_squared_distance(better_firefly)) * \
                      (better_firefly.state - self.state) + \
                      self.alpha * self.scale * np.random.uniform(-.5, .5, self.state.shape)

    def _get_squared_distance(self, better_firefly):
        return np.linalg.norm(better_firefly.state - self.state) ** 2


class RastriginFirefly(AbstractFirefly):
    def __init__(self, beta, gamma, alpha, scale, pk, **kwargs):
        self.dim = kwargs['dim']
        super().__init__(beta, gamma, alpha, scale)
        self.pk = pk

    def init_state(self):
        return np.random.uniform(-500, 500, self.dim)

    def get_optimizer(self):
        return RastriginOptimization()

    def get_initial_fitness(self):
        return np.inf


class HyperCubeRastriginFirefly(RastriginFirefly):
    def init_state(self):
        return np.random.uniform(-5.14, 5.14, self.dim)

    def get_optimizer(self):
        return HyperCubeRastriginOptimization()


class RoomFirefly(AbstractFirefly):
    def __init__(self, beta, gamma, alpha, scale, pk):
        self.furniture = self._set_up_furniture()
        self.pk = pk
        self.room_width = 40
        self.room_height = 30

        super().__init__(beta, gamma, alpha, scale)

    def init_state(self):
        for f in self.furniture:
            f.center = (np.random.uniform(0, self.room_width), np.random.uniform(0, self.room_height))

        centers = np.array([np.array(f.center) for f in self.furniture])
        return centers

    def get_optimizer(self):
        return Room(
            self.room_width, self.room_height, self.furniture,
            window=((0, 5), (0, 20)),
            door=((30, self.room_height), (38, self.room_height))
        )

    def get_initial_fitness(self):
        return - np.inf

    @staticmethod
    def _set_up_furniture():
        return [
            Furniture(center=(), width=10, height=4, name='Sofa'),
            Furniture(center=(), width=8, height=2, name='Television cabinet', can_stand_on_carpet=False),
            Furniture(center=(), width=7, height=7, name='Table with chairs', can_stand_on_carpet=False),
            Furniture(center=(), width=2, height=2, name='Armchair'),
            Furniture(center=(), width=6, height=1.5, name='Bookcase', can_stand_on_carpet=False),
            Furniture(center=(), width=4, height=2, name='Closet', can_stand_on_carpet=False),
            Furniture(center=(), width=8, height=2, name='Wardrobe', can_stand_on_carpet=False),
            Furniture(center=(), width=8, height=4, name='Coffee table'),
            Furniture(center=(), width=2, height=1, name='Clock', can_stand_on_carpet=False),
            Furniture(center=(), width=2, height=2, name='Sculpture')
        ]


class FireflyOptimization:
    def __init__(self, size, particle_class, optimization_type, beta, gamma, alpha, scale, **kwargs):
        self.swarm = [particle_class(beta, gamma, alpha, scale, pk=i, **kwargs) for i in range(size)]
        self.optimization_type = optimization_type

    def find_best_particle(self):
        return self.optimization_type(self.swarm, key=attrgetter('best_fitness'))

    def have_better_fitness(self, f1, f2):
        return {min: f1 < f2,
                max: f1 > f2}[self.optimization_type]

    def run(self, not_improving_iters):
        previous_best_fitness = 0
        counter = 0

        best = None

        while counter < not_improving_iters:
            best = self.step()

            if previous_best_fitness == best.best_fitness:
                counter += 1
            else:
                counter = 0

            previous_best_fitness = best.best_fitness

            # print(best.pk, best.best_fitness, counter)

        best.optimizer.get_fitness_value(best.best_state)
        return best

    def step(self):
        for firefly in self.swarm:
            firefly.update_fitness()

            for other_ff in self.swarm:
                other_ff.update_fitness()

                if self.have_better_fitness(other_ff.fitness, firefly.fitness):
                    firefly.move(other_ff)
                    firefly.update_fitness()

            if self.have_better_fitness(firefly.fitness, firefly.best_fitness):
                firefly.best_fitness = firefly.fitness
                firefly.best_state = copy(firefly.state)

            if firefly.alpha > 0.2:
                firefly.alpha -= 0.001

        self.swarm.sort(key=lambda x: x.fitness, reverse=self.optimization_type == max)

        return self.find_best_particle()


def draw_particles(i, ff):
    data = [particle.state for particle in ff.swarm]
    particles.set_offsets(data)
    particles.set_array(np.array([f.fitness for f in ff.swarm]))
    particles.set_cmap('hot_r')

    best = ff.step()
    print(best.pk, best.best_fitness, best.best_state, i)
    return particles,


def create_animation(filename, ff, frames):
    global particles
    axes_limit = 700
    fig = plt.figure()
    ax = fig.add_subplot(111, aspect='equal', autoscale_on=False,
                         xlim=(-axes_limit, axes_limit), ylim=(-axes_limit, axes_limit))
    ax.set_facecolor('black')

    particles = ax.scatter([], [], s=5, animated=True)
    ani = animation.FuncAnimation(fig, draw_particles, frames=frames, blit=True, fargs=(ff,))
    ani.save(filename, fps=30, extra_args=['-vcodec', 'libx264'])


def run_room_optimization(swarm_size, beta, gamma, alpha, scale, iters):
    ff = FireflyOptimization(swarm_size, particle_class=RoomFirefly, optimization_type=max, beta=beta, gamma=gamma,
                             alpha=alpha, scale=scale)

    room = ff.run(not_improving_iters=iters).optimizer

    print('Is placement legal: ', room.is_current_furniture_placement_legal())
    print(f'Angle between sofa and TV: {room.get_angle_between_furniture(room.furniture[0], room.furniture[1]):.{5}}')
    print(f'Fitness: {room.get_fitness_value()}')
    room.draw_room()


def run_create_animation(swarm_size, beta, gamma, alpha, scale, frames, filename):
    ff = FireflyOptimization(swarm_size, particle_class=RastriginFirefly, optimization_type=min,
                             beta=beta, gamma=gamma, alpha=alpha, scale=scale, dim=2)

    create_animation(filename, ff, frames)


if __name__ == '__main__':
    run_room_optimization(swarm_size=50, beta=1, gamma=.001, alpha=1, scale=10, iters=20)
    # run_create_animation(swarm_size=30, beta=1, gamma=.1, alpha=1, scale=5, frames=500, filename='ff_viz2.mp4')
