import datetime

from firefly import FireflyOptimization, RastriginFirefly, HyperCubeRastriginFirefly, RoomFirefly
from pso import PSO, RastriginParticle, HyperCubeRastriginParticle, RoomParticle
from utils import Timer
import pandas as pd
import numpy as np


def append_result(results, algorithm, problem, time, diff_to_best, swarm_size):
    results['algorithm'].append(algorithm)
    results['problem'].append(problem)
    results['time'].append(time)
    results['best'].append(diff_to_best)
    results['swarm_size'].append(swarm_size)


if __name__ == '__main__':
    not_improving_iters = 30
    swarm_size = 10
    dim = 2

    repetitions = 10

    dims = range(1, 5)
    swarm_sizes = range(10, 200, 20)

    problem_limit = 3

    timestamp = str(datetime.datetime.now())

    pso_params = {
        'size': swarm_size,
        'c_factors': (2, 2),
        'inertia': .6
    }

    firefly_params = {
        'size': swarm_size,
        'beta': 1,
        'gamma': .001,
        'alpha': 1,
        'scale': 10
    }

    rastrigin_pso = {
        'particle_class': RastriginParticle,
        'optimization_type': min,
        'dim': dim
    }

    hypercube_pso = {
        **rastrigin_pso,
        'particle_class': HyperCubeRastriginParticle,
    }

    room_pso = {
        'particle_class': RoomParticle,
        'optimization_type': max
    }

    pso_problems = [rastrigin_pso, hypercube_pso, room_pso]

    rastrigin_firefly = {
        **rastrigin_pso,
        'particle_class': RastriginFirefly
    }

    hypercube_firefly = {
        **rastrigin_firefly,
        'particle_class': HyperCubeRastriginFirefly
    }

    room_firefly = {
        **room_pso,
        'particle_class': RoomFirefly
    }

    ff_problems = [rastrigin_firefly, hypercube_firefly, room_firefly]

    results = {
        'algorithm': [],
        'problem': [],
        'time': [],
        'best': [],
        'swarm_size': []
    }

    for pso_problem, ff_problem in zip(pso_problems[:problem_limit],
                                       ff_problems[:problem_limit]):

        for swarm_s in swarm_sizes:

            pso_times = []
            pso_bests = []
            ff_times = []
            ff_bests = []

            for n in range(repetitions):
                pso_params['size'] = swarm_s
                firefly_params['size'] = swarm_s

                print(pso_problem.get('particle_class').__name__)

                pso = PSO(**pso_params, **pso_problem)
                ff = FireflyOptimization(**firefly_params, **ff_problem)

                with Timer('Elapsed') as pso_timer:
                    pso_best = pso.run(not_improving_iters=not_improving_iters)

                pso_times.append(pso_timer.time)
                pso_bests.append(pso_best.p_best_fitness)

                with Timer('Elapsed') as ff_timer:
                    ff_best = ff.run(not_improving_iters=not_improving_iters)

                ff_times.append(ff_timer.time)
                ff_bests.append(ff_best.best_fitness)

            append_result(results, 'PSO', pso_problem.get('particle_class').__name__, np.mean(pso_times),
                          np.mean(pso_bests), swarm_s)

            print(pso_bests)

            append_result(results, 'FF', ff_problem.get('particle_class').__name__, np.mean(ff_times),
                          np.mean(ff_bests), swarm_s)

            print(ff_bests)

            df = pd.DataFrame.from_dict(results)
            df.to_csv(f'measures/results_{timestamp}.csv')
