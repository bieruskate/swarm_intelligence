import itertools
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.patheffects import withStroke
from shapely.geometry import Point

from furniture import Furniture, Orientation
from problem import Problem
from utils import angle_between


class Room(Problem):
    def __init__(self, width, height, furniture, window, door):
        self.width = width
        self.height = height
        self.furniture = furniture
        self.window = window
        self.door = door
        self.overlaps = 0
        self.furniture_outside = 0

        self._sofa = next((f for f in furniture if f.name == 'Sofa'), None)
        self._tv = next((f for f in furniture if f.name == 'Television cabinet'), None)

    def get_fitness_value(self, arguments=None):
        if arguments is not None:
            for furn, center in zip(self.furniture, arguments):
                furn.center = (center[0], center[1])
                furn.orientation = self._get_furniture_orientation_depending_on_closest_wall(furn)

        is_legal, error = self.is_current_furniture_placement_legal()

        if not is_legal:
            return float(error)

        return self.carpet_radius

    def is_current_furniture_placement_legal(self):
        self.count_overlapping_furniture()
        self.count_furniture_outside_the_room()

        error_value = - (self.overlaps +
                         self.furniture_outside +
                         (not self.is_angle_between_selected_furniture_in_limit(30)) +
                         self.is_window_overlapped(3) +
                         self.is_door_overlapped(3) +
                         (self._sofa.orientation != self._tv.orientation))

        return error_value == 0, error_value

    def count_overlapping_furniture(self):
        pairs = itertools.combinations(self.furniture, 2)
        self.overlaps = sum([f1.overlap(f2) for f1, f2 in pairs])

    def count_furniture_outside_the_room(self):
        self.furniture_outside = len([
            f for f in self.furniture if not self.is_furniture_inside_room(f)])

    def is_furniture_inside_room(self, furniture):
        return (
                furniture.left >= 0 and furniture.right <= self.width and
                furniture.bottom >= 0 and furniture.top <= self.height)

    def is_window_overlapped(self, empty_range):
        (_, p1_y), (_, p2_y) = self.window

        window_center = (empty_range / 2, (p1_y + p2_y) / 2)
        window_height = np.abs(p2_y - p1_y)

        window = Furniture(window_center, empty_range, window_height)

        return self._check_specific_furniture_overlaps(window)

    def is_door_overlapped(self, empty_range):
        (p1_x, _), (p2_x, _) = self.door

        door_center = ((p2_x + p1_x) / 2, self.height - (empty_range / 2))
        door_width = np.abs(p2_x - p1_x)

        door = Furniture(door_center, door_width, empty_range)

        return self._check_specific_furniture_overlaps(door)

    def _check_specific_furniture_overlaps(self, furn):
        for f in self.furniture:
            if f.overlap(furn):
                return True

        return False

    def _get_furniture_orientation_depending_on_closest_wall(self, furniture):
        x, y = furniture.center

        horizontal_distance = min(x, self.width - x)
        vertical_distance = min(y, self.height - y)

        if horizontal_distance < vertical_distance:
            return Orientation.VERTICAL
        return Orientation.HORIZONTAL

    @property
    def carpet_radius(self):
        possible_radiuses = np.arange(.05, min(self.height, self.width) / 2, .05)
        return self._find_best_radius(possible_radiuses, best_radius=0)

    def _find_best_radius(self, radiuses, best_radius):
        if len(radiuses) == 0:
            return best_radius

        i = len(radiuses) // 2

        if not self.is_carpet_intersecting_forbidden_furniture(radiuses[i]):
            return self._find_best_radius(radiuses[i + 1:], radiuses[i])
        else:
            return self._find_best_radius(radiuses[:i], best_radius)

    def is_angle_between_selected_furniture_in_limit(self, limit):
        if not self._sofa or not self._tv:
            return True

        return self.get_angle_between_furniture(self._sofa, self._tv) <= limit

    @staticmethod
    def get_angle_between_furniture(f1, f2):
        center1 = f1.center
        center2 = f2.center

        return np.abs(90 - min(angle_between(center1, center2), angle_between(center2, center1)))

    def is_carpet_intersecting_forbidden_furniture(self, carpet_radius):
        x, y = (self.width / 2, self.height / 2)
        circle = Point(x, y).buffer(carpet_radius)
        return any([
            f.intersects_circle(circle)
            for f in self.furniture if not f.can_stand_on_carpet])

    def draw_room(self):
        room = plt.Rectangle((0, 0), self.width, self.height, fc='#cc9933')
        plt.gca().add_patch(room)

        center = (self.width / 2, self.height / 2)
        carpet = plt.Circle(center, radius=self.carpet_radius)
        plt.gca().add_patch(carpet)

        for furn in self.furniture:
            coords = furn.get_coordinates()

            s = plt.Rectangle(
                coords['bottom_left'], furn.get_width(), furn.get_height(),
                fc='purple')
            plt.gca().add_patch(s)

            txt = plt.text(furn.center[0], furn.center[1],
                           furn.name, color='white', horizontalalignment='center',
                           fontdict={'size': 9})
            txt.set_path_effects([withStroke(linewidth=1, foreground='black')])

        line = plt.Line2D(
            list(zip(*self.window))[0], list(zip(*self.window))[1], lw=4,
            color='#5f9f9f')
        plt.gca().add_line(line)

        line = plt.Line2D(
            list(zip(*self.door))[0], list(zip(*self.door))[1], lw=4,
            color='brown')
        plt.gca().add_line(line)

        plt.axis('scaled')
        plt.show()
