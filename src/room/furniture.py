from enum import Enum

from shapely.geometry import box

from utils import range_overlap


class Orientation(Enum):
    HORIZONTAL = 'horizontal'
    VERTICAL = 'vertical'


class Furniture:
    def __init__(
            self, center, width, height, name='',
            orientation=Orientation.HORIZONTAL, can_stand_on_carpet=True):
        self._width = width
        self._height = height
        self.center = center
        self.name = name
        self.orientation = orientation
        self.can_stand_on_carpet = can_stand_on_carpet

    def __str__(self):
        return self.name

    def get_coordinates(self):
        x, y = self.center

        width_range = self.get_width() / 2
        height_range = self.get_height() / 2

        coordinates = {
            'bottom_left': (x - width_range, y - height_range),
            'bottom_right': (x + width_range, y - height_range),
            'top_left': (x - width_range, y + height_range),
            'top_right': (x + width_range, y + height_range)
        }

        return coordinates

    def get_width(self):
        return (
            self._width if self.orientation == Orientation.HORIZONTAL
            else self._height)

    def get_height(self):
        return (
            self._height if self.orientation == Orientation.HORIZONTAL
            else self._width)

    @property
    def left(self):
        """Return x coordinate of left side of furniture."""
        return self.center[0] - self.get_width() / 2

    @property
    def right(self):
        """Return x coordinate of right side of furniture."""
        return self.center[0] + self.get_width() / 2

    @property
    def bottom(self):
        """Return y coordinate of bottom side of furniture."""
        return self.center[1] - self.get_height() / 2

    @property
    def top(self):
        """Return y coordinate of top side of furniture."""
        return self.center[1] + self.get_height() / 2

    def overlap(self, other):
        return (
                range_overlap(self.left, self.right, other.left, other.right) and
                range_overlap(self.bottom, self.top, other.bottom, other.top))

    def intersects_circle(self, circle):
        shape = box(self.left, self.bottom, self.right, self.top)
        return shape.intersects(circle)
