import numpy as np
from timeit import default_timer as timer


def angle_between(p1, p2):
    ang = np.arctan2(p1[1] - p2[1], p1[0] - p2[0])
    return np.rad2deg(ang % (2 * np.pi))


def range_overlap(a_min, a_max, b_min, b_max):
    """Neither range is completely greater than the other."""
    return a_min < b_max and b_min < a_max


class Timer:
    def __init__(self, msg, fmt="%0.3g"):
        self.msg = msg
        self.fmt = fmt

    def __enter__(self):
        self.start = timer()
        return self

    def __exit__(self, *args):
        t = timer() - self.start
        print(("%s : " + self.fmt + " seconds") % (self.msg, t))
        self.time = t
