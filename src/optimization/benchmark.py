from problem import Problem
import numpy as np


class RastriginOptimization(Problem):
    def get_fitness_value(self, arguments):
        return 10 * len(arguments) + np.sum([x ** 2 - 10 * np.cos(2 * np.pi * x) for x in arguments])


class HyperCubeRastriginOptimization(RastriginOptimization):
    def get_fitness_value(self, arguments):
        if not np.all([-5.12 <= x <= 5.12 for x in arguments]):
            return np.inf

        return super().get_fitness_value(arguments)
