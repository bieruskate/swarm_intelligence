from abc import ABC, abstractmethod


class Problem(ABC):

    @abstractmethod
    def get_fitness_value(self, arguments):
        pass
